###	REQUERIMIENTOS DE INSTALACION	###

Sistema Opertaivo 		: 	Windows 7,8,10 de 64 bits
		    			Ubuntu 16.04 LTS en adelante

IDE				:	Eclipse versi�n Oxygen.3a Release (4.7.3a)

					OpenJDK  9.0.4

Sistema Gestor de Base de Datos :	Postgresql 




###	CREACION DE LA ESTRUTURA DE BASE DE DATOS	###

-La creaci�n del usuario en postgres y posterior conexi�n de la base de datos con el servidor se realiza en la clase 
BDVacunaci�n

-La creacion de la base de datos y las tablas (Ciudadano e Historial) se realizan desde el shell de postgres

create database Vacunation;

create table Ciudadano (
		
		 cedula varchar2(7) not null,
		 nombre varchar2(20) not null,
		 apellido varchar2(20),
		 usuario varchar2(20),
		 contrasenia varchar2(20),
		);

create table Historial (
		
		id varchar2(7) not null,
		vacuna varchar2(20) not null,
		dosis varchar2(20),
		fecha varchar2(20),
		paciente varchar2(20),
		
		);
- Para la poblacion de los datos en la base de datos el servidor 
ofrece estos servicios en las clases PersonaDAO.java e HistorialDAO.java :
PersonaDAO 
Listar
"SELECT cedula, nombre, apellido, usuario, contrasenia FROM Ciudadano"
"SELECT cedula, nombre, apellido, usuario, contrasenia FROM ciudadano WHERE contrasenia = ? "
"SELECT cedula, nombre, apellido, usuario, contrasenia FROM ciudadano WHERE contrasenia = ? "
Insertar
INSERT INTO ciudadano(cedula,nombre,apellido,usuario,contrasenia) VALUES (?,?,?,?,?)"
Borrar
"DELETE FROM ciudadano WHERE cedula = ? "
HistorialDAO

"SELECT id, vacuna, dosis, fecha, paciente FROM Historial"
"SELECT id, vacuna, dosis, fecha, paciente FROM historial WHERE paciente = ? ";
"INSERT INTO Historial(id,vacuna,dosis,fecha,paciente)"
                + "VALUES(nextval('id_seq '),?,?,?,?)"
"UPDATE Ciudadano SET nombre = ? , apellido = ?, user=?, pass=? WHERE cedula = ? ";
"DELETE FROM Historial WHERE id = ? "
 

-Para levantar los componentes necesarios,
	1- Primero ejecutamos el servidor.
	2- Luego ejecutamos el cliente, ingresamos nuestras credenciales para poder utilizar los servicios disponibles.
	3- Luego de autenticar el servidor listara los servicios disponibles, en caso de no autenticar se negara el acceso.

###	API	###

CLASES

ServidorVacunacion: Es el servidor que ser� ejecutado y se encarga de ofrecer los diferentes servicios a los clientes (Servidores Hospital y Ciudadanos) luego de autenticar sus credenciales. Tales como :
	- Registrar una vacuna correspondiente a una persona.
	- Mostral el historial de vacuna de una persona.

Atributos:

      -puertoServidor : indica el puerto a utilizar por el servidor, con constante 8888. 
	   Tipo: int

    	-tiempo_procesamiento_miliseg = tiempo de respuesta del servidor, con constante 1000.
	   Tipo: int

ClienteTCP: El cliente le cual solicitara utilizar los servicios del servidor, enviando los datos necesarios para utilizar dichos servicios y recibir las respuestas del servidor a trav�s del puerto correspondiente.

Atributos:

	-unSocket: utilizado para establecer una conexi�n con el servidor para cada cliente que desee utilizar el servidor de vacunaci�n.
	Tipo: Socket	

- out: utilizada para escribir en formato texto en la salida estandar 	
Tipo: PrintWriter

       - in: utilizado para poder leer datos desde la entrada est�ndar.
	Tipo: BufferedReader

BDVacunacion: Es la encargada de realizar la conexi�n a la base de datos para la de los distintos servicios del servidor.

Atributos:
	- url: indica el puerto a utilizar y la base de datos a la cual se conectar� el servidor,  "jdbc:postgresql://localhost:5555/vacunation?"
	Tipo: private static final String

-	User: el usuario postgres a utilizar,"postgres".}
Tipo: private static final String

-	Password: contrase�a del usario "4054271"
Tipo: private static final String

Ciudadano : La clase para representar a una persona que  quiere utilizar el servidor y  almacenadas registros de vacunas en la base de datos.

Atributos:

-	cedula: �nico para cada persona y utilizada como clave primariaa
		Tipo: Stringg
-	nombre: nombre de una persona 
Tipo: Stringg
-	apellido: apellido de una persona;
Tipo: Stringg
	
-	usuario: nombre de usuario para poder acceder al servidorr
Tipo: Stringg

-	 contrasenia; contrase�a para poder acceder a un usuario en el servidorr
Tipo: Stringg
Metodos:

Ciudadano(){} : Constructor para crear un objeto con campos nuloss

Ciudadano(String pcedula, String pnombre, String papellido,String pusuario,String pcontrasenia){} : Constructor principall


Setters y getters correspondientes para cada atributo.

			
Historial: Es la clase utilizada para almacenar las diferentes vacunas que son aplicadas a los ciudadanos  y almacenadas en la base de datos.

Atributos:

	-id: correspondiente a un ciudadano al cual corresponde el historial.
	Tipo: Stringg

	-vacuna: nombre de la vacuna aplicada al ciudadano.
	Tipo: Stringg

	-dosis: dosis de la vacuna aplicadaa
	Tipo: Stringg

	-fecha: fecha de aplicaci�n de dicha vacunaa
	Tipo: Stringg

	-paciente: nombre del pacientee
	Tipo: Stringg

Metodos:

 Historial(){}: Constructor para crear un objeto con campos nuloss
	
Historial(String id,String Vacuna, String Dosis, String Fecha,String Paciente){} :   Constructor principall

Setter y getters correspondientes para cada atributo.

JSONcla: Clase que se encargar de la serializaci�n de datos estructurados, espec�ficamente de la clase Historial.
 Se conecta a la base de datos a trav�s de la clase BDVacunacion.

No posee atributoss

M�todos:	

String objetoString(Historial p)	: m�todo encargado de transformar un objeto Historial a String.

Historial stringObjeto(String str): m�todo encargado de transformar de String a un objeto de clase Historial.


PersonaDAO: Clase que se encarga de ofrecer el servicio de poblaci�n de datos en la base de datos, correspondiente a los ciudadanoss

No posee atributos.

M�todos: 

List<Ciudadano> seleccionar() : m�todo encargado de listar las personas que se encuentran registradas en la base de datos.

List<Ciudadano> seleccionarPorContrasenia(String contrasenia) : m�todo que se encarga de listar las personas en la base de datos filtrando por contrase�a.

public List<Ciudadano> seleccionarPorCedula(String cedula) : m�todo que se encarga de listar las personas en la base de datos filtrando por cedula.

public long insertar(Ciudadano p) throws SQLException  : se encarga de insertar en la base de datos un ciudadanoo

public long actualizar(Ciudadano p) : se encarga de actualizar en la base de datos, los respectivos cambios en un ciudadanoo

String borrar(String cedula) : se encarga de borrar un ciudadano que se encuentra en la base de datos.



HistorialDAO: : Clase que se encarga de ofrecer el servicio de poblaci�n de datos en la base de datos, correspondiente a los historiales de los pacientes


No posee atributos.

M�todos: 

List<Historial> seleccionar() : m�todo encargado de listar el historial las personas que se encuentran registradas en la base de datos.

List< Historial> seleccionarPorPaciente(String paciente) : m�todo que se encarga de listar el historial en la base de datos filtrando por paciente.


public long insertar(Historial p) throws SQLException  : se encarga de insertar en la base de datos un historiall
String borrar(String id) : se encarga de borrar un historial que se encuentra en la base de datos atraves de su id.


	
	
			



