	
public class Ciudadano {

		String cedula;
		String nombre;
		String apellido;
		String usuario;
		String contrasenia;
		public Ciudadano(){
		}

		public Ciudadano(String pcedula, String pnombre, String papellido,String pusuario,String pcontrasenia){
			this.cedula = pcedula;
			this.nombre = pnombre;
			this.apellido = papellido;
			this.usuario= pusuario;
			this.contrasenia= pcontrasenia;
		}
		
		public String getCedula() {
			return cedula;
		}

		public void setCedula(String cedula) {
			this.cedula = cedula;
		}
		public String getUsuario() {
			return usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		public String getContrasenia() {
			return contrasenia;
		}

		public void setContrasenia(String contrasenia) {
			this.contrasenia = contrasenia;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getApellido() {
			return apellido;
		}

		public void setApellido(String apellido) {
			this.apellido = apellido;
		}
}

