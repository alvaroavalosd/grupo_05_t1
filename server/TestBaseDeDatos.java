
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class TestBaseDeDatos {
	
	public List<Ciudadano> Autenticar(String user,String pass){
		PersonaDAO pdao = new PersonaDAO();
		List<Ciudadano> lista = pdao.seleccionarPorContrasenia(pass);
		return lista;
	}
	public List<Historial> ObtenerDatos(String paciente) {
		HistorialDAO pdao = new HistorialDAO();
		List<Historial> lista = pdao.seleccionarPorPaciente(paciente);
		return lista;
	}
public static void main(String args[]) throws SQLException, IOException{
		
		PersonaDAO pdao1 = new PersonaDAO();
		HistorialDAO pdao = new HistorialDAO();
		
		//pdao.insertar(new Ciudadano("4054271","Rocio", "Cabanas","RocioC","23432") );
		//pdao.insertar(new Ciudadano("4052271","Juan", "Lopez","JuanL","32542") );
		//pdao.insertar(new Ciudadano("4054241","Ana", "Iturbe","AnaI","45654") );
		//pdao.insertar(new Ciudadano("4054279","Jose", "Gomez","JoseG","34534") );
		
		
		//pdao.actualizar(new Ciudadano(,"Antonio", "Roman") );
		
		//pdao.borrar("4054271");
		
		List<Historial> lista = pdao.seleccionarPorPaciente("4054271");
		
		//List<Historial> lista = pdao.seleccionar(); 
		//List<Ciudadano> lista = pdao1.seleccionar();
		//List<Historial> lista1=pdao.seleccionar();
		
		for (Historial p: lista){
			System.out.println(p.getVacuna() + " " + p.getFecha() + ".");
		}
	}
}
