import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PersonaDAO {
	/**
	 * 
	 * @param condiciones ejemplo: "WHERE cedula = 123"
	 * @return
	 */ 
	public List<Ciudadano> seleccionar() {
		String query = "SELECT cedula, nombre, apellido, usuario, contrasenia FROM Ciudadano";
		
		List<Ciudadano> lista = new ArrayList<Ciudadano>();
		
		Connection conn = null; 
        try 
        {
        	conn = BDVacunacion.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Ciudadano p = new Ciudadano();
        		p.setCedula(rs.getString(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		p.setUsuario(rs.getString(4));
        		p.setContrasenia(rs.getString(5));
        		
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public List<Ciudadano> seleccionarPorContrasenia(String contrasenia) {
		String SQL = "SELECT cedula, nombre, apellido, usuario, contrasenia FROM ciudadano WHERE contrasenia = ? ";
		
		List<Ciudadano> lista = new ArrayList<Ciudadano>();
		
		Connection conn = null; 
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1, contrasenia);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Ciudadano p = new Ciudadano();
        		p.setCedula(rs.getString(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		p.setUsuario(rs.getString(4));
        		p.setContrasenia(rs.getString(5));
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	public List<Ciudadano> seleccionarPorCedula(String cedula) {
		String SQL = "SELECT cedula, nombre, apellido, usuario, contrasenia FROM ciudadano WHERE contrasenia = ? ";
		
		List<Ciudadano> lista = new ArrayList<Ciudadano>();
		
		Connection conn = null; 
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Ciudadano p = new Ciudadano();
        		p.setCedula(rs.getString(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		p.setUsuario(rs.getString(4));
        		p.setContrasenia(rs.getString(5));
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
    public long insertar(Ciudadano p) throws SQLException {

        String SQL = "INSERT INTO ciudadano(cedula,nombre,apellido,usuario,contrasenia) VALUES (?,?,?,?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, p.getCedula());
            pstmt.setString(2, p.getNombre());
            pstmt.setString(3, p.getApellido());
            pstmt.setString(4, p.getUsuario());
    		pstmt.setString(5, p.getContrasenia());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la insercion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;   	
    	
    }
    /*public long actualizar(Ciudadano p) throws SQLException {

        String SQL = "UPDATE Ciudadano SET nombre = ? , apellido = ?, user=?, pass=? WHERE cedula = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, p.getNombre());
            pstmt.setString(2, p.getApellido());
            pstmt.setString(3, p.getCedula());
            pstmt.setString(4,p.getUser());
    		pstmt.setString(5,p.getPass());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }*/
    
    public String borrar(String cedula) throws SQLException { 

        String SQL = "DELETE FROM ciudadano WHERE cedula = ? ";
 
        String id = "0";
        Connection conn = null;
        
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, cedula);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getString(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la eliminación: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
}	
