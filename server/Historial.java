

public class Historial {
	
	String id;
	String vacuna;
	String dosis;
	String fecha;
	String paciente;
	
	public Historial(){
	}
	
	public Historial(String id,String Vacuna, String Dosis, String Fecha,String Paciente){
		this.id=id;
		this.vacuna= Vacuna;
		this.dosis = Dosis;
		this.fecha = Fecha;
		this.paciente= Paciente;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getVacuna() {
		return vacuna;
	}

	public void setVacuna(String vacuna) {
		this.vacuna = vacuna;
	}
	public String getDosis() {
		return dosis;
	}

	public void setDosis(String dosis) {
		this.dosis = dosis;
	}
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}
	

}
