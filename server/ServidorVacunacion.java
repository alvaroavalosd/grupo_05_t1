import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.*;
import java.text.SimpleDateFormat;
import java.io.*;
import java.util.Date;

public class ServidorVacunacion {
	public static void main(String[] args) throws Exception {

        int puertoServidor = 8888;
        int tiempo_procesamiento_miliseg = 1000;
		
		try{
			tiempo_procesamiento_miliseg = Integer.parseInt(args[0]);
		}catch(Exception e1){
			System.out.println("Se omite el argumento, tiempo de procesamiento " + tiempo_procesamiento_miliseg  + ". Ref: " + e1);
		}
		
		
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(puertoServidor);
        } catch (IOException e) {
            System.err.println("No se puede abrir el puerto: " +puertoServidor+ ".");
            System.exit(1);
        }
        System.out.println("Puerto abierto: "+puertoServidor+".");
        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Fallo el accept().");
            System.exit(1);
        }
        
        Logger logger = Logger.getLogger("MyLog");  
        FileHandler fh;  

        try {  

            // This block configure the logger with handler and formatter  
            fh = new FileHandler("C:\\Users\\Jarvis\\Desktop\\Sockets\\TP1SDII\\src\\MyLogFile.log");  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  

            // the following statement is used to log any messages  

        } catch (SecurityException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  

        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        out.println("Bienvenidos. a)Registrar Vacuna b) Ver Libreta de Vacunacion c) Salir:");
        String inputLine, outputLine="";

        inputLine = in.readLine();
        System.out.println("Mensaje recibido: " + inputLine);
        
        TimeUnit.MILLISECONDS.sleep(tiempo_procesamiento_miliseg);
        
        if(inputLine.equals("a")) {
        	String user,pass;
        	out.println("Ingresar User:");
        	user = in.readLine();
        	out.println("Ingresar pass");
        	pass = in.readLine();
        	
        TestBaseDeDatos T = new TestBaseDeDatos();
        	List<Ciudadano> L = T.Autenticar(user,pass); 
        	if(L.isEmpty())
    		 out.println("Acceso Denegado");
    		else {
    			for (Ciudadano p: L){
    				if(user.equals(p.getUsuario())){
    					out.println("Acceso Autorizado. Ingresar Vacuna");
    					String vac = in.readLine();
    		        	out.println("Ingresar Dosis:");
    		        	String dosis = in.readLine();
    		        	HistorialDAO H = new HistorialDAO();
    		        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    		        	String fecha = sdf.format(new Date());
    		        	System.out.println(vac + " " + dosis + " " + fecha);
    		        	Historial H1= new Historial();
    		            H1.setVacuna(vac);
    		            H1.setDosis(dosis);
    		            H1.setFecha(fecha);
    		            H1.setPaciente(p.getCedula());
    		            H.insertar(H1);
    		        	out.println("Vacuna Registrada");
    		        	logger.info("Vacuna Registrada");  
    				}else
    					out.println("Acceso Denegado");
    			}
    		}
                	
        } else if(inputLine.equals("b")) {
        	String ci;
        	out.println("Ingresar cedula del Paciente");
        	ci=in.readLine();
        	HistorialDAO H = new HistorialDAO();
        	List<Historial> L = H.seleccionarPorPaciente(ci);
        	logger.info("Libreta de Vacunacion enviada");  
        	for(Historial h:L) {
        	JSONcla RepreString = new JSONcla();
        	String r1 = RepreString.objetoString(h);
        	out.println(r1);
        	ci = in.readLine();       	
        	}

}
        
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }

}
