import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Hospital {
	public static void main(String[] args) throws Exception {

        Socket unSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            unSocket = new Socket("localhost",8888);
            
            // enviamos nosotros
            out = new PrintWriter(unSocket.getOutputStream(), true);

            //viene del servidor
            in = new BufferedReader(new InputStreamReader(unSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Host desconocido");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Error de I/O en la conexion al host");
            System.exit(1);
        }

        
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String fromServer;
        String fromUser;
        
        while ((fromServer = in.readLine())!=null) {
        	if(fromServer.equals("Bienvenidos. a)Registrar Vacuna b) Ver Libreta de Vacunacion c) Salir:") || fromServer.equals("Ingresar cedula del Paciente")) {
        	System.out.println("Servidor: " + fromServer);
        	if (fromServer.equals("Bye")) {
        					break;
            }
            fromUser = stdIn.readLine();
            if (fromUser != null) {
                System.out.println("Cliente: " + fromUser);

                //escribimos al servidor
                out.println(fromUser);
            }
        	}else {
        		JSONcla H = new JSONcla();
        		Historial h = H.stringObjeto(fromServer);
        		System.out.println("Servidor: Vacuna:" + h.getVacuna() + ", Dosis:" + h.getDosis() + ", Fecha:" + h.getFecha());
        		out.println("");
        	}
       }
        out.close();
        in.close();
        stdIn.close();
        unSocket.close();
    }


}
