
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONcla {

    public String objetoString(Historial p) {	
    	
		JSONObject obj = new JSONObject();
        obj.put("vacuna", p.getVacuna());
        obj.put("dosis", p.getDosis());
        obj.put("fecha", p.getFecha());
        obj.put("paciente",p.getPaciente());

        return obj.toJSONString();
    }
    
    
 public Historial stringObjeto(String str) throws Exception {
    	Historial p = new Historial();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;

        String vacuna = (String) jsonObject.get("vacuna");
        p.setVacuna(vacuna);
        p.setDosis((String)jsonObject.get("dosis"));
        p.setFecha((String)jsonObject.get("fecha"));
        p.setPaciente((String)jsonObject.get("paciente"));
        
        return p;
	}

}
