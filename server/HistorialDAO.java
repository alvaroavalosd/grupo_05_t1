import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class HistorialDAO {
 
	public List<Historial> seleccionar() {
		String query = "SELECT id, vacuna, dosis, fecha, paciente FROM Historial";
		
		List<Historial> lista = new ArrayList<Historial>();
		
		Connection conn = null; 
        try 
        {
        	conn = BDVacunacion.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Historial p = new Historial();
        		p.setVacuna(rs.getString(1));
        		p.setDosis(rs.getString(2));
        		p.setFecha(rs.getString(3));
        		p.setPaciente(rs.getString(4));
       		
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public List<Historial> seleccionarPorPaciente(String paciente) {
		String SQL = "SELECT id, vacuna, dosis, fecha, paciente FROM historial WHERE paciente = ? ";
		
		List<Historial> lista = new ArrayList<Historial>();
		
		Connection conn = null; 
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1,paciente);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Historial p = new Historial();
        		p.setVacuna(rs.getString(2));
        		p.setDosis(rs.getString(3));
        		p.setFecha(rs.getString(4));
        		p.setPaciente(rs.getString(5));
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
    public long insertar(Historial p) throws SQLException {

        String SQL = "INSERT INTO Historial(id,vacuna,dosis,fecha,paciente)"
                + "VALUES(nextval('id_seq '),?,?,?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
        	//pstmt.setString(1,p.getId());
        	pstmt.setString(1, p.getVacuna());
            pstmt.setString(2, p.getDosis());
            pstmt.setString(3, p.getFecha());
            pstmt.setString(4, p.getPaciente());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la insercion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;   	
    	
    }
    public long actualizar(Historial p) throws SQLException {

        String SQL = "UPDATE Ciudadano SET nombre = ? , apellido = ?, user=?, pass=? WHERE cedula = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
        	pstmt.setString(1,p.getId());
        	pstmt.setString(2, p.getVacuna());
            pstmt.setString(3, p.getDosis());
            pstmt.setString(4, p.getFecha());
            pstmt.setString(5,p.getPaciente());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    public String borrar(String id) throws SQLException { 

        String SQL = "DELETE FROM Historial WHERE id = ? ";
 
        String ID;
        Connection conn = null;
        
        try 
        {
        	conn = BDVacunacion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, id);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        ID = rs.getString(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la eliminación: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
}	
