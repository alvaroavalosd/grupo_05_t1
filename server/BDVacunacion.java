
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BDVacunacion {
	private static final String url = "jdbc:postgresql://localhost:5555/vacunation?";
    private static final String user = "postgres";
    private static final String password = "4054271";
    /**
     * @return objeto Connection 
     */
    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}